<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>CONTACT FORM</title>
</head>

<body>
    <h1>Contact Form</h1>
    <div class="container">
        <div class="row my-5">
            <div class="col-md-12">

            <h2>UPDATE CONTACT</h2>

                <?php

                include 'db.php';
                $id = $_GET['id'];

                $sql = "SELECT * FROM contacts WHERE id = " . $id;

                $result = mysqli_query($conn, $sql);

                if ($result) {
                    $row = mysqli_fetch_assoc($result);
                    $name = $row['name'];
                    $phone = $row['phone'];
                    $email = $row['email'];
                    $address = $row['address'];
                    $image = $row['image'];
                }

                ?>
                <form action="editaction.php" method="POST" enctype="multipart/form-data">
                    <div class="main">
                        <label for="name">
                            <h4 class="text-danger">Fullname</h4>
                        </label><br>
                        <input type="text" name="name" value="<?php global $name; echo $name ?>" required><br><br>

                        <label for="phone">
                            <h4 class="text-danger">Phone No.</h4>
                        </label><br>
                        <input type="number" name="phone" value="<?php global $phone; echo $phone ?>" required><br><br>

                        <label for="email">
                            <h4 class="text-danger">Email</h4>
                        </label><br>
                        <input type="email" name="email" value="<?php global $email; echo $email ?>" required><br><br>

                        <label for="address">
                            <h4 class="text-danger">Address</h4>
                        </label><br>
                        <input type="text" name="address" value="<?php global $address; echo $address ?>" required><br><br>

                        <label for="image">
                            <h4 class="text-danger">Image</h4>
                        </label><br>

                        <input type="file" name="image" value="<?php global $image; echo $image ?>" required><br><br>

                        <input type="hidden" name="id" value="<?php global $id; echo $id ?>" required>

                        <input type="submit" name="update" value="Update">

                    </div>
                </form>

            </div>
        </div>
    </div>

</body>

</html>