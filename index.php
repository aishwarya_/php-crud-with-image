<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>CONTACT FORM</title>
</head>

<body>
    <h1>Contact Form</h1>
    <div class="container">
        <div class="row my-5">
            <div class="col-md-12">
                <form action="adddata.php" method="POST" enctype="multipart/form-data">
                    <div class="main">
                        <label for="name">
                            <h4 class="text-danger">Fullname</h4>
                        </label><br>
                        <input type="text" name="name" required><br><br>

                        <label for="phone">
                            <h4 class="text-danger">Phone No.</h4>
                        </label><br>
                        <input type="number" name="phone" required><br><br>

                        <label for="email">
                            <h4 class="text-danger">Email</h4>
                        </label><br>
                        <input type="email" name="email" required><br><br>

                        <label for="address">
                            <h4 class="text-danger">Address</h4>
                        </label><br>
                        <input type="text" name="address" required><br><br>

                        <label for="image">
                            <h4 class="text-danger">Image</h4>
                        </label><br>
                        <input type="file" name="image" required><br><br>

                        <input type="submit" name="upload" value="Save">

                    </div>
                </form>

                <hr>

                <table class="border">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Phone No.</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Image</th>
                        <th>Action</th>

                    </tr>

                    <?php

                    include 'db.php';

                    $sql = "SELECT * FROM contacts ORDER BY id DESC";

                    $result = mysqli_query($conn, $sql);

                    // var_dump($result);

                    if ($result) {
                        while ($row = mysqli_fetch_assoc($result)) {
                            $id = $row['id'];
                            $name = $row['name'];
                            $phone = $row['phone'];
                            $email = $row['email'];
                            $address = $row['address'];
                            $image = $row['image'];

                    ?>

                            <tr>
                                <td><?php echo ++$i ?></td>
                                <td><?php echo $name ?></td>
                                <td><?php echo $phone ?></td>
                                <td><?php echo $email ?></td>
                                <td><?php echo $address ?></td>

                                <td>
                                    <img src="<?php echo 'images/' . $row['image']; ?>" width="100px" height="100px" alt="Image">
                                </td>

                                <td>
                                    <a href="edit.php?id=<?php echo $id ?>">Update</a>
                                    <a href="delete.php?id=<?php echo $id ?>">Delete</a>

                                </td>

                            </tr>

                    <?php
                        }
                    }

                    ?>

                    </tr>
                </table>

            </div>
        </div>
    </div>

</body>

</html>